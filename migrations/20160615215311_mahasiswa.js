'use strict';

function createMhs(table) {
    table.increments('id').primary();
    table.string('nrp').unique().notNullable();
    table.string('nama');
    table.timestamps();
}


exports.up = function(knex, Promise) {
  return Promise.all([
      knex.schema.createTable('mahasiswa', createMhs),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
      knex.schema.dropTable('mahasiswa'),
  ]);
};
