// Update with your config settings.
const mkuConfig = require('./config/dev.local');
const mkuConfigServer = require('./config/prod');

module.exports = {

    localnet: {
        client: 'postgresql',
        connection: mkuConfig.postgres.connectionUri,
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
        },
    },

    server: {
        client: 'postgresql',
        connection: mkuConfigServer.postgres.connectionUri,
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
        },
    },

    development: {
        client: 'sqlite3',
        connection: {
            filename: './dev.sqlite3',
        },
    },

    staging: {
        client: 'postgresql',
        connection: {
            database: 'my_db',
            user:     'username',
            password: 'password',
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
        },
    },

    production: {
        client: 'postgresql',
        connection: {
            database: 'my_db',
            user:     'username',
            password: 'password',
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: 'knex_migrations',
        },
    },

};
