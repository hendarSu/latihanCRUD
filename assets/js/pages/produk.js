/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableProduk = function() {
        window.tableProduk = jQuery('.data-produk').dataTable({
            order: [[2, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1, 4] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: '/produk/get',
            columns: [
                    { data: 'id',
                      render:function(data, type, row) {
                          var str = '<div class="checkbox-produk">' +
                                        '<input type="checkbox" name="produkId" value="' + data + '" data-toggle="tooltip" title="Select to remove">' +
                                  '</div>';
                          return str;
                      },
                    },
                    { data: 'foto',
                      render: function(data, type, row) {
                          var str = '<img src="' + data + '" width="50" style="border-radius:5%">';
                          return str;
                      },
                    },
                    { data: 'judul' },
                    { data: 'jenis' },
                    { data: 'status' },
                    { data: 'id',
                      render: function(data, type, row) {
                          var str = '<a href="/ubah/penduduk/' + data + '"><button class="btn btn-default" data-toggle="tooltip" title="Ubah Data Penduduk"><i class="si si-pencil"></i></button></a>';
                          return str;
                      },
                    },

                  ],
        }).on('draw.dt', function() {
            $(this).removeAttr('style');
            $('[data-toggle="tooltip"]').tooltip();
            $('.checkbox-produk').shiftcheckbox({
                checkboxSelector: ':checkbox',
                selectAll: $('.checkbox-produk-all'),
                ignoreClick: 'a',
                onChange: function(checked) {

                },
            });
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend(true, $DataTable.defaults, {
            dom:
                '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
                '<\'row\'<\'col-sm-12\'tr>>' +
                '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: '_MENU_',
                sInfo: 'Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>',
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>',
                },
            },
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: 'dataTables_wrapper form-inline dt-bootstrap',
            sFilterInput: 'form-control',
            sLengthSelect: 'form-control',
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function(settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function(container, buttons) {
                var i, ien, node, button;
                var clickHandler = function(e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    } else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                class: classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                tabindex: settings.iTabIndex,
                                id: idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null,
                            })
                            .append(jQuery('<a>', {
                                    href: '#',
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                container: 'DTTT btn-group',
                buttons: {
                    normal: 'btn btn-default',
                    disabled: 'disabled',
                },
                collection: {
                    container: 'DTTT_dropdown dropdown-menu',
                    buttons: {
                        normal: '',
                        disabled: 'disabled',
                    },
                },
                print: {
                    info: 'DTTT_print_info',
                },
                select: {
                    row: 'active',
                },
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                collection: {
                    container: 'ul',
                    button: 'li',
                    liner: 'a',
                },
            });
        }
    };

    var initValidationAdd = function() {
        jQuery('.form-add-produk').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                ImageFile: {
                    required: true,
                },
                judul: {
                    required: true,
                },
                jenis: {
                    required: true,

                },
                status: {
                    required: true,

                },
                deskripsi: {
                    required: true,
                },
            },
            messages: {
                ImageFile: {
                    required: 'pilih salah satu gambar',
                },
                judul: {
                    required: 'judul harus disisi',
                },
                jenis: {
                    required: 'Pilih salah satu jenis!',

                },
                deskripsi: {
                    required: 'Deskripsi harus diisi!',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();
                            $('#deskripsi-add-produk').code('');
                            $('#imgPreview').attr('src', '/img/default-paper.png');
                            $('#modal-succes .block-content p').text("Data Produk Berhasil Disimpan");
                            $('#modal-succes').modal('show');
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Simpan Produk');
                    },

                    error: function(jqXHR, exception) {
                      console.log(jqXHR)
                        $('#modal-notif .block-content p').text(jqXHR.responseJSON.data);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Simpan Produk');;
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    var initValidationUpdate = function() {
        jQuery('.form-update-penduduk').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    minlength: 5,
                },
                repassword: {
                    minlength: 5,
                    equalTo: '#password-update-employee',
                },
            },
            messages: {
                name: {
                    required: 'Please enter a name',
                },
                email: 'Please enter a valid email address',
                password: {
                    minlength: 'Your password must be at least 5 characters long',
                },
                repassword: {
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();
                            $('#imgPreview-update').attr('src', '/img/default.png')
                            $('#modal-update-employee').modal('hide');
                            window.tableProduk.api().ajax.reload();
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Save');
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Save');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {
            // Init Datatables
            initValidationAdd();
            initValidationUpdate();
            bsDataTables();
            initDataTableProduk();
        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});

/* PROCCESSING */

// add
$('input.upload').on('change', function(e) {
    if (this.files && this.files[0].name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
        var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
        var reader = new FileReader();
        reader.onload = function(e) {
            image.attr('src', e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
        $('#cropper-modal').modal('show');
    }else {
        alert('file not supported');
    }
});

$('#cropper-modal').on('shown.bs.modal', function() {
    var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
    image.cropper({
        // aspectRatio: 3 / 4,
        autoCropArea: 0.5,
        cropBoxResizable: true,
        checkImageOrigin: true,
        responsive: true,
        built: function() {
            // Strict mode: set crop box data first
            image.cropper('setCropBoxData', cropBoxData);
            image.cropper('setCanvasData', canvasData);
        },
    });
});


$('.btn-crop').on('click', function(e) {
    var imgb64 = $('#cropper-wrap-img > img').cropper('getCroppedCanvas').toDataURL('image/jpeg');
    $('img#imgPreview').attr('src', imgb64);
    $('#srcDataCrop').val(imgb64);
    $('img#imgPreview-update').attr('src', imgb64);
    $('#srcDataCrop-update').val(imgb64);
    $('#cropper-modal').modal('hide');
});

// edit
$('#cropper-modal').on('hidden.bs.modal', function() {
    $('#cropper-wrap-img > img').cropper('destroy');
    // $('body').addClass('modal-open');
});

// $('#modal-add-employee').on('hidden.bs.modal', function() {
//     $('img#imgPreview').attr('src', '/img/default.png');
//     $('#srcDataCrop').val('');
// });

$('#modal-update-employee').on('hidden.bs.modal', function() {
    $('img#imgPreview-update').attr('src', '/img/default.png');
    $('#srcDataCrop-update').val('');
});

// remove
$(document).on('change', '.data-produk input:checkbox', function() {
    if ($('.data-produk input:checkbox:checked').length > 0) {
        $('.btn-delete-produk').removeAttr('disabled');
    }else {
        $('.btn-delete-produk').attr('disabled', 'disabled');
    }
});

$('.btn-delete-produk').click(function(e) {
    if ($('.data-produk input:checkbox:checked').length > 0) {
        var conf = confirm('Hapus Data Produk yang dipilih ?');
        if (conf) {
            var data = $('.data-produk input:checkbox:checked').serialize();

            $.ajax({
                url:'/produk/remove',
                type:'POST',
                data:data,
                success: function(res) {
                    if (res.data == '1') {
                        window.tableProduk.api().ajax.reload();
                        $('.btn-delete-produk').attr('disabled', 'disabled');
                        $('.data-produk input:checkbox:checked').removeAttr('checked');
                    }else {
                        $('#modal-notif .block-content p').text(res.data);
                        $('#modal-notif').modal('show');
                    }
                },

                error: function(jqXHR, exception) {
                    $('#modal-notif .block-content p').text(jqXHR.status);
                    $('#modal-notif').modal('show');
                },
            });
        }
    }else {
        alert('tolong pilih salah satu produk untuk menghapus!');
    }
});
