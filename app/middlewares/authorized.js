'use strict';

exports.isAuthorized = function() {

    return Async.route(function *(req, res, next) {
        if (!Utils.hasProperty(req.query, ['token'])) return res.ok('Unauthorized', 'failed', 401);
        var token =  yield Token.where({token: req.query.token}).fetch().catch(console.error);
        if (!token) return res.ok('unauthorized access', 'failed', 401);
        next();
    });
};
