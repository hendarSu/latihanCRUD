'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'mahasiswa',
    hasTimestamps: true,
});
