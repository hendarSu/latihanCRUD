
const express = require('express');
const router = express.Router();

router.get('/', MahasiswaController.index);
router.get('/tambah/mahasiswa', MahasiswaController.addMhs);
router.get('/mahasiswa/get', MahasiswaController.get);
router.get('/mahasiswa/get/:id', MahasiswaController.dataSingle);
router.post('/mahasiswa/remove', MahasiswaController.remove);
router.post('/mahasiswa/add', MahasiswaController.add);
router.post('/mahasiswa/update/:id', MahasiswaController.remove);

module.exports = router;
