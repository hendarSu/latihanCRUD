'use strict';

const express = require('express');
const router = express.Router();
const auth = require('../middlewares/authorized');

// router.get('/', auth.isAuthorized(), (req, res, next) => {
//     res.ok('Access forbidden');
// });
//
// // Token
// router.get('/token/:serverKey', TokenController.get);
//
// // Member
// router.get('/member/register', auth.isAuthorized(), MemberController.register);
//
// // Worship
// router.get('/worship/register', auth.isAuthorized(), RegWorshipController.register);
//
// // Travel
// router.get('/travel/:travelId', auth.isAuthorized(), TravelController.get);
//
// // Albums
// router.get('/albums/:travelId', auth.isAuthorized(), AlbumController.get);
//
// //Medias
// // router.get('/medias/:albumId', auth.isAuthorized(), MediaController.get);
// router.get('/medias/:type', auth.isAuthorized(), MediaController.get);
//
// // News
// router.get('/news/:travelId', auth.isAuthorized(), NewsController.get);
// router.get('/news/detail/:id', auth.isAuthorized(), NewsController.getDetail);
//
// // Promotions
// router.get('/promotions/:travelId', auth.isAuthorized(), PromotionController.get);
//
// // Testimonials
// router.get('/testimonials/:travelId', auth.isAuthorized(), TestimonialController.get);
//
// // Info Ibadah
// router.get('/hajji/:travelId', auth.isAuthorized(), InfoIbadahController.hajji);
// router.get('/umroh/:travelId', auth.isAuthorized(), InfoIbadahController.umroh);


module.exports = router;
