'use strict';

const Mahasiswa = require('../models/Mahasiswa');

module.exports = bookshelf.Collection.extend({
    model: Mahasiswa,
});
