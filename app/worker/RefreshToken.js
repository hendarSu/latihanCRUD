const Promise = require('bluebird')
const schedule = require('node-schedule');
var rule = new schedule.RecurrenceRule();

rule.hour = 00;
rule.minute = 01;

module.exports = schedule.scheduleJob(rule, wrap(function *() {
    const newToken = Utils.uid(16);
    const data = {
        token: newToken,
    };
    const token = yield Token.forge({id: 1}).fetch().catch(console.error);

    const updated = yield token.save(data).catch(console.error);
}));

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function() {
        cr().catch();
    };
}
