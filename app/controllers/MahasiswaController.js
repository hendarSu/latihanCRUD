'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/mahasiswa', model);
    }),

    addMhs: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/add_mahasiswa', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Mahasiswa, 'datatables').forge()
        .paginate({
            request: req,
            // withRelated: ['desa'],
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var mahasiswa =  yield Mahasiswa.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(mahasiswa.toJSON());
   }),


    add: Async.route(function *(req, res, next) {
        const temp = yield new Mahasiswa().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);
        const tempJson = temp.toJSON();

        const id = parseInt(tempJson.max) + 1;
        if (id > 1 ) {
          req.body.id = id;
        }else{
          req.body.id = 1;
        }

        const mahasiswa = Mahasiswa.forge();
        const added = yield mahasiswa.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Gagal menambahkan data Mahasiswa');
        }

        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {

        if (req.body.mahasiswaId.constructor === Array) {
            _.forEach(req.body.mahasiswaId, wrap(function *(v, i) {

                var mahasiswa = yield Mahasiswa.forge({id: v}).fetch().catch(console.error);
                var mahasiswaData = mahasiswa.toJSON();

                const deleted = yield mahasiswa.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete employee Failed');
                }
            }));

            return res.ok('1');

        }else {

            var mahasiswa = yield Mahasiswa.forge({id: req.body.mahasiswaId}).fetch().catch(console.error);
            var mahasiswaData = mahasiswa.toJSON();
            const deleted = yield mahasiswa.destroy().catch(console.error);
            if (!_.isEmpty(deleted)) {

            }

            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {

       var mahasiswa = yield Mahasiswa.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield mahasiswa.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Employees');
       }

       return res.ok('1');
   }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
